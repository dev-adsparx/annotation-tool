import os
import shutil
import time
from datetime import datetime
from subprocess import call
import streamlit as st
import matplotlib.pyplot as plt

import m3u8
import requests
import wget

from utils import loggers as lg

class daiManifest:

	def __init__(self, vodSize, liveUrl):
		self.start = True
		self.lastMediaSeq = 0
		self.timeElapsed = 0
		self.vodSize = vodSize
		# self.vodSize = 300  # in seconds
		self.refreshRate = 4
		self.segpath = 'vod/'
		self.VoDManifest = '/home/abcd/Documents/AdSparx/annotation-tool/vod.m3u8'
		self.basepath = '/home/abcd/Documents/AdSparx/annotation-tool/'
		self.duration = 6.06
		self.custom = False
		self.liveUrl = liveUrl
		# self.liveUrl = 'https://plutotvliv.vo.llnwd.net/c/tyt2/playlist720p.m3u8'
		# self.liveUrl = 'https://plutotvliv.vo.llnwd.net/c/people/chunklist_b1428000.m3u8'

	def dai_vod(self):
		st.write('Beginning Manifest creation. Refer to logs/tool.log for high level of verbose')
		progress = int((self.timeElapsed/self.vodSize)*100)
		print(progress)
		progress_bar = st.progress(progress)

		f = open(self.VoDManifest, 'a+')
		if not os.path.exists(self.segpath):
			os.mkdir(self.segpath)
		else:
			shutil.rmtree(self.segpath)
			os.mkdir(self.segpath)

		if not os.path.exists('logs'):
			os.mkdir('logs')

		call('rm -rf *.tmp', shell=True)
		lg.tool.info('\n\n\t-------- STARTING NEW MANIFEST CREATION --------\n\n')

		while self.timeElapsed <= self.vodSize:
			if self.start:

				try:
					obj = m3u8.load(self.liveUrl)
				except Exception as e:
					lg.tool.error(f'Error while fetching initial segments: {e}', exc_info=True)
					lg.tool.debug('Retrying fetching segments...')
					obj = m3u8.load(self.liveUrl)

				obj.dump(self.VoDManifest)
				lg.tool.info('Initial segments: \n %s', obj.segments)
				for seg in obj.segments:
					segUrl = str(seg.absolute_uri)
					segName = os.path.split(segUrl)[-1]
					self.duration = seg.duration
					self.timeElapsed += seg.duration
					progress = int((self.timeElapsed / self.vodSize) * 100)
					progress_bar.progress(progress)
					if not self.segExists(segName):
						try:
							t1 = datetime.now()
							# wget.download(segUrl, out=self.segpath, bar=None)
							r = requests.get(segUrl)
							t2 = datetime.now()
							lg.tool.info('Time to download %s: %s', segName, str(t2 - t1))
							with open(self.basepath + segName, 'wb') as seg:
								seg.write(r.content)

						except ValueError:
							t1 = datetime.now()
							r = requests.get(segUrl)
							# wget.download(segUrl, out=self.segpath, bar=None)
							t2 = datetime.now()
							segUrl = r.url
							seg.absolute_uri = segUrl
							lg.tool.info('\nActual Ad Url: %s', segUrl)
							with open(self.basepath + segName, 'wb') as seg:
								seg.write(r.content)
							lg.tool.info('Time to download seg --> %s', str(t2 - t1))
							lg.tool.info('Ad segment added to mainfest!')

				obj.dump(self.VoDManifest)
				self.start = False
				self.lastMediaSeq = obj.media_sequence

			try:
				obj = m3u8.load(self.liveUrl)
			except Exception as e:
				lg.tool.error(f'Error while fetching initial segments: {e}', exc_info=True)
				lg.tool.debug('Retrying fetching segments...')
				obj = m3u8.load(self.liveUrl)

			segs = obj.segments
			currMediaSeq = obj.media_sequence
			diff = currMediaSeq - self.lastMediaSeq
			lg.tool.info('\n\nNew segments fetched!\n%s\n', obj.segments)
			if diff > len(segs):
				skip = diff - len(segs)
				lg.tool.warning('%d segments will be skipped!', skip)
				diff = 10

			lg.tool.info('Curr-Last = %d - %d', currMediaSeq, self.lastMediaSeq)
			lg.tool.info('# of segements to fetch = %d', diff)

			for i in range(diff, 0, -1):
				segDur = segs[-i].duration
				segUrl = segs[-i].absolute_uri
				segName = os.path.split(segUrl)[-1]
				lg.tool.info('Segment name --> %s', segName)
				self.timeElapsed += segDur
				progress = int((self.timeElapsed / self.vodSize) * 100)
				progress_bar.progress(progress)
				lg.tool.info('Time Elapsed = %f', self.timeElapsed)

				if not self.segExists(segName):
					try:
						t1 = datetime.now()
						r = requests.get(segUrl)
						t2 = datetime.now()
						# wget.download(segUrl, out=self.segpath, bar=None)
						with open(self.basepath + segName, 'wb') as seg:
							seg.write(r.content)
						f.write(str(segs[-i]) + '\n')
						lg.tool.info('Time to download %s: %s', segName, str(t2 - t1))
						lg.tool.info('Segment added to manifest!')

					except ValueError as e:
						st.exception('Exception occured while recording %s'%e)
						lg.tool.debug('Retrying due to ValueError: %s', e)
						r = requests.get(segUrl)
						segUrl = r.url
						segs[-i].absolute_uri = segUrl
						lg.tool.info('\nActual Ad Url: %s', segUrl)
						t1 = datetime.now()
						wget.download(segUrl, out=self.segpath, bar=None)
						t2 = datetime.now()
						lg.tool.info('Time to download segment --> %s', str(t2 - t1))
						f.write(str(segs[-i]) + '\n')
						lg.tool.info('Ad segment added to manifest!')

				else:
					lg.tool.info('Already exists! Skipping %s', segName)

			if diff <= 1:
				lg.tool.info('~~~~ Sleeping for %d ~~~~', self.refreshRate)
				time.sleep(self.refreshRate)
				lg.tool.info('~~~~ Waking up ~~~~')

			else:
				lg.tool.info('~~~~ Not sleeping! ~~~~')
			self.lastMediaSeq = currMediaSeq

		st.text('Thread stopped! Manifest created!')
		lg.tool.info('Thread stopped! Manifest created!')
		f.close()
	# self.changeUrl()

	def changeUrl(self):

		try:
			obj = m3u8.load(self.VoDManifest)
			segs = obj.segments
			lg.tool.info('\n\nCreating manifest with new segment urls')
			lg.tool.info('Length of segs = %d', len(segs))
			for seg in segs:
				url = seg.uri

				# To handle urls w/o ?
				segName = os.path.split(url)[-1]

				# To handle seg urls with ?
				# segName = os.path.split(url.split('?')[0])[-1]
				# segName = url.split('?')[0]

				lg.tool.info('%s added to maifest', segName)
				seg.uri = self.basepath + segName

			obj.dump('new.m3u8')
			f = open('new.m3u8', 'a')
			f.write('#EXT-X-ENDLIST\n')
			if self.custom:
				lg.tool.info('Changing custom ad cues in new manifest...')
				self.customMarkerHandler('new.m3u8')
			lg.tool.info('New manifest created!')

		except ValueError as err:
			self.custom = True
			lg.tool.debug('ValueError:%s',err)
			lg.tool.info('Manifest contains non-standard tags...')
			self.customMarkerHandler(self.VoDManifest)
			self.changeUrl()

	def customMarkerHandler(self, manifest):
		new_lines = []
		with open(manifest, 'r') as src:
			vod = src.readlines()
			for lineno, line in enumerate(vod, 1):
				if line.find('.ts') == -1 and line.find(':') == -1 and line.find('CUE') != -1:
					lg.tool.debug('Custom tag found at line#%d - %s' % (lineno, line))
					line = line.rstrip('\n')
					line += ':%0.03f' % self.duration + '\n'
					lg.tool.debug('Changed to: %s', line)
				new_lines.append(line)
		with open(manifest, 'w+') as src:
			src.writelines(new_lines)
			lg.tool.info('New file created!')

	def segExists(self, segName):
		return os.path.isfile(self.basepath + segName)

