from datetime import datetime
from subprocess import call

import pandas as pd
import streamlit as st

from utils import loggers as lg


class PreProcess:
	"""

	A class that performs that handles the required pre-processing for the the Testing/Testing

	Attributes
	----------

	cols : list
		Name of the features to be used for the current Machine Learning workflow

	Sub-classes
	-----------

	Test :
		Performs the pre-processing for test data

	Train :
		Performs the neccessary pre-processing for training the classifier

	"""

	all_cols = 	['luma_avg', 'luma_change_ratio', 'kld_hist_2frames', 'vertical_black_values',
			 	'horizontal_black_values', 'crU_change_ratio', 'crV_change_ratio', 'crU_avg',
			 	'crV_avg', 'AvgB', 'AvgG', 'AvgR', 'KLD_histB', 'KLD_histG', 'KLD_histR', 'KLD_hist_Pattern', 'Time']

	cols = 	['luma_avg', 'luma_change_ratio', 'kld_hist_2frames', 'vertical_black_values',
			 'horizontal_black_values', 'crU_change_ratio', 'crV_change_ratio', 'crU_avg',
			 'crV_avg', 'AvgB', 'AvgG', 'AvgR', 'KLD_histB', 'KLD_histG', 'KLD_histR', 'KLD_hist_Pattern', 'Time']

	def __init__(self):
		pass

	class Test:

		"""
		Performs the pre-processing for test data

		Attributes
		----------

		in_vid : str
			path to the test asset

		filename : str
			name of the asset extracted from the in_vid path

		Methods
		-------

		compute_metrics()
			Calls AdSparx's videometrics filter through ffmpeg, for test data

		clean()
			Read the metrics csv and performs pre-processing on the data

		"""

		def __init__(self, in_vid, filename):
			self.in_vid = in_vid
			self.filename = filename
			# self.filename = os.path.split(self.in_vid)[-1].split(".")[0]
			lg.tool.debug('Features used %s', PreProcess.cols)

		def compute_metrics(self):
			"""
			Computes AdSparx's videometrics for the test asset.

			Returns
			-------

			metrics_df : pandas.dataframe
				A cleaned dataframe containing the metrics of the test asset

			filename : str
				Name of the corresponding asset

			"""

			in_vid = self.in_vid
			cmd1 = "ffmpeg -threads 4 -i " + '"%s"' % in_vid + " -vf videometrics=config_file=./utils/videometrics.cfg -f null - > ./logs/ffmpeg_log.txt 2>&1"
			cmd2 = 'paste -d"," luma_avg.dat luma_change_ratio.dat kld_hist_2frames.dat vertical_black_values.dat horizontal_black_values.dat crU_change_ratio.dat ' \
				   'crV_change_ratio.dat crU_avg.dat crV_avg.dat avgB.dat avgG.dat avgR.dat kld_histB.dat kld_histG.dat kld_histR.dat kld_hist_pattern.dat time.dat > %s.csv' \
				   % self.filename
			st.write(f'Features being computed: {PreProcess.cols}')

			try:
				lg.tool.info('Trying to compute metrics for %s' % in_vid)
				t1 = datetime.now()
				call(cmd1, shell=True)
				t2 = datetime.now()
				call(cmd2, shell=True)
				lg.tool.info('Cleaning the metrics csv now...')
				metrics_df = self.clean()

				call("rm -rf *.dat", shell=True)
				st.text(f'Time taken to compute metrics {str(t2-t1)}')
				lg.tool.debug('Time to compute metrics: %s', str(t2 - t1))
				lg.tool.info('-----------> %s done <-----------', self.filename)

				return metrics_df, self.filename

			except Exception as e:
				lg.tool.error('-------------- ERROR in PreProcessing --------------')
				lg.tool.error('Error in computing metrics: %s', e, exc_info=True)
				return 'Error occurred: ' + str(e)

		def clean(self):
			"""
			Adds column names to the dataframe, for the features and metrics computed

			Returns
			-------

			metrics_df : pandas.dataframe
				A cleaned dataframe containing the metrics of the test asset

			"""

			metrics_df = pd.read_csv(self.filename + '.csv', names=PreProcess.cols)
			lg.tool.info('metrics_df \n%s ', metrics_df.head(10))
			metrics_df = metrics_df.dropna()
			st.write('Metrics dataframe: ')
			metrics_df.to_csv(self.filename + '.csv', index=False)
			lg.tool.info('Cleaned metrics_df \n%s ', metrics_df.head(10))
			return metrics_df