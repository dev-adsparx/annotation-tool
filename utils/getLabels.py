import os
import re
import sys
from subprocess import check_output, call
import subprocess
import numpy as np
import pandas as pd
import time

import m3u8

from utils import loggers as lg

class Annotater:

	def __init__(self):
		pass

	def magic(self, manifest):
		lg.tool.info('Reading manifest to find Ad Markers...')
		labels = []
		obj = m3u8.load(manifest)
		lg.tool.info('Manifest loaded. Reading segments...')
		allsegs = obj.segments

		for num, seg in enumerate (allsegs):
			start, vFrames = self.getSegStats(seg.absolute_uri)

			if seg.cue_out:
				lg.tool.info('Ad Segment found at segment#%d : %s', num, seg)
				ys = np.ones(vFrames)
			else:
				ys = np.zeros(vFrames)
			labels.append(ys)
		lg.tool.info('All segments parsed.')

		df = pd.read_csv('tyt.csv')
		df['Labels'] = labels

		if not len(labels) == df.index:
			lg.tool.warning('Labels and row count do not match. FATAL ERROR.')
			lg.tool.warning('*JETPACKS AWAY FROM THIS*')
			sys.exit(1)

		df.to_csv('new.csv', index=False)
		lg.tool.info('Dataset is ready for training.')
		lg.tool.info('And remember Peter: With great amounts of data comes great training!')

	@staticmethod
	def getSegStats(segURL):
		segName = os.path.split(segURL)[-1]
		# startPTS = str(check_output(['./utils/parser %s | grep PTS_START' % segURL], shell=True))
		# startPTS = str(check_output(['./utils/shell.sh %s | grep PTS_START' % segURL], shell=True))
		time.sleep(1)

		startPTS = os.popen('./utils/shell.sh %s | grep PTS_START' % segURL).read()

		# startPTS = subprocess.Popen(['./utils/shell.sh', '%s'%segURL], stdout=subprocess.PIPE)
		print(startPTS)
		# print('------------------->',int(startPTS))
		# startPTS = (re.findall(r'[0-9]+', startPTS)[0])
		# vFrames = str(check_output(['./utils/parser %s | grep vFRAMES' % segURL], shell=True))
		# vFrames = str(os.popen('./utils/shell.sh %s | grep vFRAMES' % segURL).read())
		vFrames = subprocess.run(['./utils/shell.sh', '%s' % segURL, '|', 'grep', 'vFRAMES'],stdout=subprocess.PIPE).stdout
		print(vFrames)
		vFrames = (re.findall(r'[0-9]+', vFrames)[0])
		lg.tool.info('Stats for %s: startPTS = %d vFRAMES = %d',segName, startPTS, vFrames)
		return startPTS, vFrames
