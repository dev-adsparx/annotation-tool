import smtplib
import logging
from logging.handlers import SMTPHandler, RotatingFileHandler

class SSLSMTPHandler(SMTPHandler):
	def emit(self, record):
		"""
        Emit a record.
        """
		try:
			port = self.mailport
			if not port:
				port = smtplib.SMTP_PORT
			smtp = smtplib.SMTP_SSL(self.mailhost, port)
			msg = self.format(record)
			if self.username:
				smtp.login(self.username, self.password)
			smtp.sendmail(self.fromaddr, self.toaddrs, msg)
			smtp.quit()
		except (KeyboardInterrupt, SystemExit):
			raise
		except:
			self.handleError(record)

def setup_logger(name, filename):
	"""

	The basic configuration for logging of the MiDAS process

	Parameters
	----------

	name : str
		Name of the logger object, usually will be train/test

	filename : str
		Name and location of the logging file

	request_id : str
		Unique request ID for a particular asset

	Returns
	-------

	logger : logging object
		The logger object created with the specified configuration.

	"""

	file_logger = logging.FileHandler(filename, mode='a')
	# file_logger = RotatingFileHandler(filename, mode='a', maxBytes=1*1024*1024, backupCount=100)
	formatter = logging.Formatter(
		'<%(asctime)s> [%(levelname)s] <%(lineno)s> <%(funcName)s> %(message)s',
		datefmt='%m/%d/%Y %I:%M:%S %p')
	file_logger.setFormatter(formatter)

	logger = logging.getLogger(name)
	logger.setLevel(logging.DEBUG)
	logger.addHandler(file_logger)

	mail_handler = SSLSMTPHandler(mailhost=('mail.adsparx.in', 587),
                           		  fromaddr='alerts@adsparx.com',
                           		  toaddrs=['darpan@adsparx.in','lilesh@adsparx.in', 'kunal@adsparx.in'],
                           		  subject='Error alert from MiDAS VoD Server',
                           		  credentials=('alerts@adsparx.com', 'Mystique@786'))
	mail_format = logging.Formatter('''
									Message type:       %(levelname)s
									Location:           %(pathname)s:%(lineno)d
									Function:           %(funcName)s
									Time:               %(asctime)s
									Message:			%(message)s
									''',
									datefmt='%m/%d/%Y %I:%M:%S %p')

	mail_handler.setFormatter(mail_format)
	mail_handler.setLevel(logging.ERROR)
	logger.addHandler(mail_handler)

	return logger
