# annotation-tool

Tool to convert live stream with SCTE markers to data from videometrics filter for ad-detection training.

Requirements: ``` pip3 install -r requirements.txt ```

Usage: ``` streamlit run ui.py ```

