import streamlit as st
from utils.daiManifest import daiManifest
from utils.preprocess import PreProcess
from utils.getLabels import Annotater

st.title('Stream Annotation tool')
st.write('Select VOD Size in minutes')
vodSize = st.slider('VOD Size',min_value=1, max_value=100, step=5)
vodSize = vodSize*60
st.write('Enter source URL with SCTE markers')
liveUrl = st.text_input('Source URL')
st.markdown('**VoD of %d minutes from %s will be created.**'%(vodSize/60,liveUrl))
click = st.button('START')

if vodSize and liveUrl and click:
	manifest = daiManifest(vodSize, liveUrl)
	manifest.dai_vod()
	st.write('VOD Manifest created')
	st.balloons()
	st.write('Changing relative web urls to local absolute urls')
	manifest.changeUrl()
	st.write('Calculating metrics for the VdD')
	pp = PreProcess.Test('new.m3u8', 'tyt')
	pp.compute_metrics()
	annotate = Annotater()
	annotate.magic('new.m3u8')


