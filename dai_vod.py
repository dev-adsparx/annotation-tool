from utils.daiManifest import daiManifest
from utils.preprocess import PreProcess
from utils.getLabels import Annotater

if __name__ == '__main__':
	manifest = daiManifest()
	manifest.dai_vod()
	manifest.changeUrl()
	pp = PreProcess.Test('new.m3u8', 'tyt')
	pp.compute_metrics()
	annotate = Annotater()
	annotate.magic('new.m3u8')
